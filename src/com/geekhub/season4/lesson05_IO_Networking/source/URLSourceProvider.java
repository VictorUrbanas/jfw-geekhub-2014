package com.geekhub.season4.lesson05_IO_Networking.source;


import com.geekhub.season4.lesson05_IO_Networking.InvalidSourceException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * Implementation for loading content from specified URL.<br/>
 * Valid paths to load are http://someurl.com, https://secureurl.com, ftp://ftpurl.com etc.
 */
public class URLSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        //TODO: implement me
        boolean success = true;
        try {
            new URL(pathToSource).openConnection();
        } catch (IOException e) {
            success = false;
        }
        return success;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        //TODO: implement me
        StringBuilder textForTranslated = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        new URL(pathToSource).openConnection().getInputStream()))) {
            String readLine = null;
            while ((readLine = bufferedReader.readLine()) != null) {
                textForTranslated.append(readLine);
            }
        }
        catch (IOException e) {
            throw new InvalidSourceException("Cannot load text from source!");
        }
        return textForTranslated.toString();
    }
}
