package com.geekhub.season4.lesson05_IO_Networking.source;

import com.geekhub.season4.lesson05_IO_Networking.InvalidSourceException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Implementation for loading content from local file system.
 * This implementation supports absolute paths to local file system without specifying file:// protocol.
 * Examples c:/1.txt or d:/pathToFile/file.txt
 */
public class FileSourceProvider implements SourceProvider {

    @Override
    public boolean isAllowed(String pathToSource) {
        //TODO: implement me
        boolean success = true;
        try {
            new InputStreamReader(Files.newInputStream(Paths.get(pathToSource)));
        } catch (Exception e) {
            success = false;
        }
        return success;
    }

    @Override
    public String load(String pathToSource) throws IOException {
        //TODO: implement me
        StringBuilder stringBuilder = new StringBuilder();
        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(Files.newInputStream(Paths.get(pathToSource))))) {
            String readLine = null;
            while ((readLine = bufferedReader.readLine()) != null) {
                stringBuilder.append(readLine);
            }
            bufferedReader.close();
        } catch (IOException e) {
            throw new InvalidSourceException("Cannot load text form file!");
        }
        return stringBuilder.toString();
    }
}
