package com.geekhub.season4.lesson05_IO_Networking.source;

import com.geekhub.season4.lesson05_IO_Networking.InvalidSourceException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * SourceLoader should contains all implementations of SourceProviders to be
 * able to load different sources.
 */
public class SourceLoader {
    private List<SourceProvider> sourceProviders = new ArrayList<>();

    public SourceLoader() {
        //TODO: initialize me
        sourceProviders.add(new URLSourceProvider());
        sourceProviders.add(new FileSourceProvider());
    }

    public String loadSource(String pathToSource) throws IOException {
        //TODO: implement me
        String textForTranslate = null;
        for (SourceProvider sourceProvider : sourceProviders) {
            if (sourceProvider.isAllowed(pathToSource)) {
                textForTranslate = sourceProvider.load(pathToSource);
                break;
            }
        }
        if (textForTranslate == null) {
            throw new InvalidSourceException("Invalid source!");
        }
        return textForTranslate;
    }
}
