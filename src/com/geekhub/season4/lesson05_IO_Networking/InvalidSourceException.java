package com.geekhub.season4.lesson05_IO_Networking;

import java.io.IOException;

/**
 * Created by Vik on 28.11.2014.
 */
public class InvalidSourceException extends IOException {
    public InvalidSourceException(String message) {
        super(message);
    }
}
