package com.geekhub.season4.lesson05_IO_Networking;

import java.io.IOException;

/**
 * Created by Vik on 28.11.2014.
 */
public class ImpossibleTranslateException extends IOException {
    public ImpossibleTranslateException(String message) {
        super(message);
    };
}
