package com.geekhub.season4.lesson05_IO_Networking;


import com.geekhub.season4.lesson05_IO_Networking.source.URLSourceProvider;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Provides utilities for translating texts to russian language.<br/>
 * Uses Yandex Translate API, more information at <a href="http://api.yandex.ru/translate/">http://api.yandex.ru/translate/</a><br/>
 * Depends on {@link com.geekhub.season4.lesson05_IO_Networking.source.URLSourceProvider} for accessing Yandex Translator API service
 */
public class Translator {
    /**
     * Yandex Translate API key could be obtained at <a href="http://api.yandex.ru/key/form.xml?service=trnsl">http://api.yandex.ru/key/form.xml?service=trnsl</a>
     * to do that you have to be authorized.
     */
    private static final String YANDEX_API_KEY = "trnsl.1.1.20141128T143951Z.6ba697aeeab4588c.ba12c622caddc36100b6d56c8f71bb9c97fa63b2";
    private static final String TRANSLATION_DIRECTION = "ru";
    private URLSourceProvider urlSourceProvider;

    public Translator(URLSourceProvider urlSourceProvider) {
        this.urlSourceProvider = urlSourceProvider;
    }

    /**
     * Translates text to russian language
     *
     * @param original text to translate
     * @return translated text
     * @throws IOException
     */
    public String translate(String original) throws IOException {
        //TODO: implement me
        String urlForTranslate = prepareURL(original);
        if (!urlSourceProvider.isAllowed(urlForTranslate)) {
            throw new ImpossibleTranslateException("Can't be translated " +
                    "(problems with URL for translate)!");
        }

        return parseContent(urlSourceProvider.load(urlForTranslate));
    }

    /**
     * Prepares URL to invoke Yandex Translate API service for specified text
     *
     * @param text to translate
     * @return url for translation specified text
     */
    private String prepareURL(String text) {
        return "https://translate.yandex.net/api/v1.5/tr/translate?key=" +
                YANDEX_API_KEY + "&text=" + encodeText(text) + "&lang=" +
                TRANSLATION_DIRECTION;
    }

    /**
     * Parses content returned by Yandex Translate API service. Removes all tags
     * and system texts. Keeps only translated text.
     *
     * @param content that was received from Yandex Translate API by invoking
     *                prepared URL
     * @return translated text
     */
    private String parseContent(String content) {
        //TODO: implement me
        String textTag = "text>";
        int lengthTextTag = textTag.length();
        int beginIndex = content.indexOf(textTag) + textTag.length();
        int endIndex = content.indexOf("/" + textTag) - 1;
        return content.substring(beginIndex, endIndex);
    }

    /**
     * Encodes text that need to be translated to put it as URL parameter
     *
     * @param text to be translated
     * @return encoded text
     */
    private String encodeText(String text) {
        //TODO: implement me
        String encodeURL = null;
        try {
            encodeURL = URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Sorry, but your URL cannot be encode!");
        }
        return encodeURL;
    }
}
