package com.geekhub.season4.lesson05_IO_Networking;


import com.geekhub.season4.lesson05_IO_Networking.source.SourceLoader;
import com.geekhub.season4.lesson05_IO_Networking.source.URLSourceProvider;

import java.io.IOException;
import java.util.Scanner;

public class TranslatorController {

    public static void main(String[] args) {
        //initialization
        SourceLoader sourceLoader = new SourceLoader();
        Translator translator = new Translator(new URLSourceProvider());

        Scanner scanner = new Scanner(System.in);
        System.out.println("Set URL or PATH to local file on your computer for " +
                "translation! Be careful with spaces, replace all spaces by %20.");
        String command = scanner.next();
        while (!"exit".equals(command)) {
            //TODO: add exception handling here to let user know about it and ask him to enter another path to translation
            //      So, the only way to stop the application is to do that manually or type "exit"
            System.out.format("Processing...\nPlease, wait\n");
            String source = null;
            String infoMessage = null;
            try {
                source = sourceLoader.loadSource(command);
                String translation = null;
                translation = translator.translate(source);
                System.out.println("Original: " + source);
                System.out.println("Translation: " + translation);
                infoMessage = "Successfully!";
            } catch (InvalidSourceException e) {
                infoMessage = e.getMessage();
            } catch (ImpossibleTranslateException e) {
                infoMessage = e.getMessage();
            } catch (IOException e) {
                infoMessage = "Fatal Error!";
                e.printStackTrace();
            } finally {
                System.out.format("%s\nSet URL or PATH again or type \"exit\"\n", infoMessage);
            }
            command = scanner.next();
        }
    }
}
