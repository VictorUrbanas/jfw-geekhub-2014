package com.geekhub.season4.lesson02_OOP;

import java.util.Random;
import java.util.Scanner;

/**
 * Created by Vik on 27.10.2014.
 */
public class SolarPoweredMachine extends Vehicle {
    private Engine engine;
    private boolean antenna;
    private boolean flying;
    private String destination;
    private boolean solarBattery;
    private boolean brokenDown;

    public SolarPoweredMachine(String vehicleType) {
        super(vehicleType);
        antenna = true;
        solarBattery = true;
        brokenDown = false;
        flying = false;
        activation();
    }

    @Override
    public void activation() {
        engine = new Engine(50);
        System.out.println("A new solar powered machine bought");
        gettingRout();
    }

    @Override
    public void acceleration() {
        if (brokenDown || engine.isDestroyed()) {
            brokenDown = true;
            System.out.println("Satellite has broken down!");
        } else {
            System.out.println("Accelerate...");
            engine.upTemperature(20);
            switch ((new Random()).nextInt(15)) {
                case 0:
                    antenna = false;
                    break;
                case 1:
                    solarBattery = false;
                    break;
                case 2:
                    brokenDown = true;
                    System.out.println("Satellite has broken down! " +
                            "Connection destroyed!");

            }
        }

    }

    @Override
    public void turning(String rout) {
        if (brokenDown || engine.isDestroyed()) {
            brokenDown = true;
            System.out.println("Satellite has broken down!");
        } else {
            destination = rout;
            flying = true;
            System.out.println("Course is set");
            satelliteData();
            System.out.println("Flying...");
            engine.upTemperature(5);
        }
    }

    @Override
    public void stop() {
        if (brokenDown) {
            System.out.println("Satellite has broken down!");
        } else {
            if (flying) {
                System.out.println("Landing on the " + destination + "...");
                flying = false;
                engine.downTemperature(25);
            } else {
                engine.downTemperature(5);
                System.out.println("Inspection stopped");
            }
        }
    }

    @Override
    public void inspectMechanism() {
        System.out.println("Vehicle inspection!");
        if (!engine.isDestroyed() && !brokenDown) {
            System.out.println("Everything is fantastic!");
        } else {
            if (engine.isDestroyed()) {
                System.out.println("Engine destroyed!");
            }

            if (!solarBattery) {
                System.out.println("Solar battery destroyed!");
            }
            if (!antenna) {
                System.out.println("Antenna destroyed!");
            }
            brokenDown = true;
            System.out.println("Satellite has broken down!");
        }
        System.out.println("Inspection ended");
    }

    @Override
    public boolean driving(boolean accelerate) {
        if (brokenDown || engine.isDestroyed()) {
            brokenDown = true;
            System.out.println("Satellite has broken down!");
            return false;
        }
        if (flying) {
            System.out.println("We are flying to the " + destination +
                    "\n Do you want to change the rout? :\n" +
                    "1. Yes\n" +
                    "2. No");
            Scanner scanner = new Scanner(System.in);
            switch (scanner.nextInt()) {
                case 1:
                    gettingRout();
                    break;
                default:
                    System.out.println("Continue flying...");
                    engine.upTemperature(5);

            }
            return false;
        }
        System.out.println("Inspection the " + destination);
        engine.upTemperature(15);
        switch ((new Random()).nextInt(15)) {
            case 0:
                antenna = false;
                System.out.println("Antenna has broken down!");
                brokenDown = true;
                break;
            case 1:
                solarBattery = false;
                System.out.println("SolarBattery has broken down!");
                brokenDown = true;
                break;

        }
        return true;
    }

    private void gettingRout() {
        System.out.println("What planet do you want to inspect?");
        this.turning(destination = new Scanner(System.in).next());
    }

    private void satelliteData() {
        System.out.printf("Satellite data:\nengine temperature = %d\nantenna " +
                        "%s\nsolar battery %s\n", engine.getTemperature() + 5,
                antenna ? "is not damaged" : "is broken!",
                solarBattery ? "is not damaged" : "is broken!");
    }
}
