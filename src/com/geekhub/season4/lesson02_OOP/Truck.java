package com.geekhub.season4.lesson02_OOP;

import java.util.Random;

/**
 * Created by Vik on 26.10.2014.
 */
public class Truck extends Vehicle {
    private Engine engine;
    private Wheels wheels;
    private GasTank gasTank;

    public Truck(String vehicleType) {
        super(vehicleType);
        System.out.println("New " + vehicleType + " bought!");
        activation();
    }

    @Override
    public void acceleration() {
        System.out.printf("Warning!\nTemperature of your engine will increase " +
                "significantly\nYour truck will burn a lot of fuel!\n");
        driving(true);
    }

    @Override
    public void turning(String rout) {
        System.out.println("Turning to the " + rout);
    }

    @Override
    public void stop() {
        if (engine.isWorking()) {
            engine.setWorking(false);
            engine.downTemperature(50);
            System.out.println("Truck stopped!");
        } else {
            System.out.println("Truck is already stopped! Maybe you want to drive?");
        }

    }

    @Override
    public void activation() {
        engine = new Engine(130);
        wheels = new Wheels(6);
        gasTank = new GasTank(170);
        System.out.println("Yor truck is now ready to drive!");
    }

    @Override
    public boolean driving(boolean accelerate) {
        engine.setWorking(true);
        if (!gasTank.isFull()) {
            System.out.println("It's a pity but gas-tank is empty. You need refuel!");
            return false;
        }
        if (wheels.isDestroy()) {
            System.out.println("It seems that some wheel is bursted");
            return false;
        }
        if (engine.isDestroyed()) {
            System.out.println("Engine destroyed! Your truck needs the " +
                    "inspectMechanism");
            return false;
        }
        System.out.println("Driving...");
        System.out.println(accelerate ? "Accelerate..." : "Accelerate for fast " +
                "riding! Just do it :)");
        engine.upTemperature(accelerate ? 40 : 20);
        gasTank.burnFuel(accelerate ? 50 : 40);
        wheelsStatus();
        System.out.printf("--\nengine temperature = %d\nfuel quantity = %d\n--\n",
                engine.getTemperature(), gasTank.getFuelValue());
        return true;
    }

    @Override
    public void inspectMechanism() {
        int numberOfService = 1;
        System.out.println("Vehicle inspection!");
        if (!gasTank.isFull()) {
            gasTank.refuel();
            System.out.println(numberOfService++ + ". Refuel");
        }
        if (engine.isDestroyed()) {
            engine = new Engine(130);
            System.out.println(numberOfService++ + ". New engine bought");
        }
        if (wheels.isDestroy()) {
            wheels.setDestroy(false);
            System.out.println(numberOfService + ". Wheels repaired");
        }
        System.out.println("Inspection ended");
    }

    private void wheelsStatus() {
        if ((new Random()).nextInt(wheels.getNumberOfWheels()) < 1) {
            wheels.setDestroy(true);
            System.out.println("BANG! Check the wheel because it seems that it bursted");
        }
    }
}
