package com.geekhub.season4.lesson02_OOP;

/**
 * Created by Vik on 26.10.2014.
 */
public class GasTank {
    private int capacity;
    private int fuelValue;
    private boolean empty;

    public GasTank(int capacity) {
        this.capacity = capacity;
        empty = false;
        fuelValue = capacity;
    }

    public boolean isFull() {
        return !empty;
    }

    public int getFuelValue() {
        return fuelValue;
    }

    public void refuel() {
        empty = false;
        fuelValue = capacity;
    }

    public void upFuel(int increment) {
        fuelValue = fuelValue + increment > capacity ? capacity :
                fuelValue + increment;
        empty = false;
    }

    public void burnFuel(int decrement) {
        fuelValue -= fuelValue - decrement >= 0 ? decrement : fuelValue;
        if (fuelValue <= 0) {
            empty = true;
        }
    }
}
