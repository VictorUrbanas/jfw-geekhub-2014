package com.geekhub.season4.lesson02_OOP;

import java.util.Scanner;

/**
 * Created by Vik on 26.10.2014.
 */
public class Travelling {
    static boolean exit = false;

    public static void main(String[] args) {
        while (!exit) {
            startTravel();
        }
    }

    public static void startTravel() {
        System.out.println("\nChoose option:\n" +
                "1. Take a truck\n" +
                "2. Take a boat\n" +
                "3. Take a satellite\n" +
                "4. Exit");
        switch (new Scanner(System.in).nextInt()) {
            case 1:
                travelling(new Truck("truck"));
                break;
            case 2:
                travelling(new Boat("boat Fury XL Sport"));
                break;
            case 3:
                travelling(new SolarPoweredMachine("satellite Mars Exploration " +
                        "Rover"));
                break;
            case 4:
                exit = true;
                break;
            default:
                System.out.println("Illegal choise");
        }
    }

    public static void travelling(Vehicle vehicle) {
        while (!exit) {
            System.out.println("\nChoose option:\n" +
                    "1. Drive\n" +
                    "2. Accelerate\n" +
                    "3. Turn\n" +
                    "4. Stop\n" +
                    "5. Vehicle inspection\n" +
                    "6. Exit");
            switch (new Scanner(System.in).nextInt()) {
                case 1:
                    try {
                        vehicle.driving(false); //driving without acceleration
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    try {
                        vehicle.acceleration();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 3:
                    System.out.println("Please set the rout for the turning:");
                    try {
                        vehicle.turning(new Scanner(System.in).next());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 4:
                    vehicle.stop();
                    break;
                case 5:
                    vehicle.inspectMechanism();
                    break;
                case 6:
                    exit = true;
                    break;
                default:
                    System.out.println("Illegal choise");
            }
        }
    }
}
