package com.geekhub.season4.lesson02_OOP;

/**
 * Created by Vik on 26.10.2014.
 */
public interface Drivable {
    boolean driving(boolean accelerate) throws Exception;

    void activation();
}
