package com.geekhub.season4.lesson02_OOP;

import java.util.Random;

/**
 * Created by Vik on 26.10.2014.
 */
public class Boat extends Vehicle {
    private Engine engine;
    private GasTank gasTank;
    private boolean hasHoleBottom;
    private boolean drowned;

    public Boat(String vehicleType) {
        super(vehicleType);
        hasHoleBottom = false;
        drowned = false;
        activation();
    }

    @Override
    public boolean driving(boolean accelerate) {
        engine.setWorking(true);
        if (drowned) {
            System.out.println("If you want boating, you must buy a new boat, " +
                    "past boat was drowned :(");
            return false;
        }
        if (engine.isDestroyed()) {
            System.out.println("Engine destroyed!");
            return false;
        }
        if (!gasTank.isFull()) {
            System.out.println("It's a pity but gas-tank is empty. You need refuel!");
            return false;
        }
        if (hasHoleBottom) {
            drowned = true;
            System.out.println("we have saved you, but your boat is drowned!");
            return false;
        }
        System.out.println("Boating...");
        System.out.println(accelerate ? "Accelerate..." : "Accelerate for fast " +
                "boating! Just do it :)");
        gasTank.burnFuel(accelerate ? 5 : 2);
        engine.upTemperature(accelerate ? 30 : 15);
        checkBottom();
        System.out.printf("--\nengine temperature = %d\nfuel quantity = %d\n--\n",
                engine.getTemperature(), gasTank.getFuelValue());
        return true;
    }

    @Override
    public void activation() {
        engine = new Engine(85);
        gasTank = new GasTank(20);
        System.out.println("The new boat bought!");
    }

    @Override
    public void acceleration() {
        System.out.printf("Warning!\nTemperature of your engine will increase " +
                "significantly\nYour boat will burn a lot of fuel!\n");
        driving(true);
    }

    @Override
    public void turning(String rout) {
        System.out.println("Turning to the " + rout);
    }

    @Override
    public void stop() {
        if (engine.isWorking()) {
            engine.setWorking(false);
        }
        engine.downTemperature(30);
        System.out.println("Going with the stream");
    }

    @Override
    public void inspectMechanism() {
        int numberOfService = 1;
        System.out.println("Vehicle inspection!");
        if (!gasTank.isFull()) {
            gasTank.refuel();
            System.out.println(numberOfService++ + ". Refuel");
        }
        if (engine.isDestroyed()) {
            engine = new Engine(85);
            System.out.println(numberOfService++ + ". New engine bought");
        }
        if (hasHoleBottom && !drowned) {
            hasHoleBottom = false;
            System.out.println(numberOfService + ". Bottom was repaired");
        } else {
            System.out.println(numberOfService + ". New boat bought");
            hasHoleBottom = false;
            drowned = false;
            gasTank = new GasTank(20);
            engine = new Engine(85);
            activation();
        }
        System.out.println("Inspection ended");
    }

    private void checkBottom() {
        if ((new Random()).nextInt(10) < 2) {
            hasHoleBottom = true;
            System.out.println("Ooops, there is a hole in your bottom, repair " +
                    "your boat, hurry up!");
        }
    }
}
