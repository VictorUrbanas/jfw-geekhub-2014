package com.geekhub.season4.lesson02_OOP;

/**
 * Created by Vik on 26.10.2014.
 */
public class Engine {
    private boolean working;
    private int temperature = 0;
    private int criticalTemperature;
    private boolean destroyed;

    public Engine(int criticalTemperature) {
        working = false;
        destroyed = false;
        this.criticalTemperature = criticalTemperature;
    }

    public boolean isWorking() {
        return working;
    }

    public void setWorking(boolean working) {
        this.working = working;
    }

    public void upTemperature(int increment) {
        temperature += increment;
        if (temperature >= criticalTemperature) {
            destroyed = true;
            working = false;
            System.out.println("Oh, no! Your engine is broke from overheating!");
        }
    }

    public int getTemperature() {
        return temperature;
    }

    public void downTemperature(int decrement) {
        temperature -= decrement;
    }

    public boolean isDestroyed() {
        return destroyed;
    }
}
