package com.geekhub.season4.lesson02_OOP;

/**
 * Created by Vik on 26.10.2014.
 */
public class Wheels {
    private int numberOfWheels;
    private boolean destroy;

    public Wheels(int numberOfWheels) {
        this.numberOfWheels = numberOfWheels;
        destroy = false;
    }

    public void setDestroy(boolean destroy) {
        this.destroy = destroy;
    }

    public boolean isDestroy() {
        return destroy;
    }

    public int getNumberOfWheels() {
        return numberOfWheels;
    }
}
