package com.geekhub.season4.lesson02_OOP;

/**
 * Created by Vik on 26.10.2014.
 */
public abstract class Vehicle implements Drivable {
    private String vehicleType;

    public Vehicle(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public abstract void acceleration() throws Exception;

    public abstract void turning(String rout) throws Exception;

    public abstract void stop();

    public abstract void inspectMechanism();

    public abstract boolean driving(boolean accelerate) throws Exception;
}