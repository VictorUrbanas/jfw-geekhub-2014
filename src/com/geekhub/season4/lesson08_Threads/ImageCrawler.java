package com.geekhub.season4.lesson08_Threads;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * ImageCrawler downloads all images to specified folder from specified resource.
 * It uses multi threading to make process faster. To start download images you
 * should call downloadImages(String urlToPage) method with URL.
 * To shutdown the service you should call stop() method
 */
public class ImageCrawler {
    //number of threads to download images simultaneously
    public static final int NUMBER_OF_THREADS = 10;
    private ExecutorService executorService = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
    private String folder;

    public ImageCrawler(String folder) throws MalformedURLException {
        this.folder = folder;
    }

    /**
     * Call this method to start download images from specified URL.
     *
     * @param urlToPage
     * @throws IOException
     */
    public void downloadImages(String urlToPage) throws IOException {
        //implement me
        try {
            URL url = new URL(urlToPage);
            if (isImageURL(url)) {
                executorService.submit(new ImageTask(url, folder));
            } else {
                Page webPage = new Page(url);
                for (URL imageURL : webPage.getImageLinks()) {
                    System.out.println("May be an URL? " + imageURL);
                    if (isImageURL(imageURL)) {
                        executorService.submit(new ImageTask(imageURL, folder));
                    }
                }
            }
        } catch (MalformedURLException e) {
            System.out.println("Wrong URL format! URL: " + urlToPage);
        }
    }

    /**
     * Call this method before shutdown an application
     */
    public void stop() {
        executorService.shutdown();
    }

    //detects is current url is an image. Checking for popular extensions should be enough
    private boolean isImageURL(URL url) {
        //implement me
        return url.getPath().toLowerCase().matches("(.*)(\\.png|\\.bmp|\\.gif|\\.ico|\\.jpeg|\\.jpg)$");
    }


}
