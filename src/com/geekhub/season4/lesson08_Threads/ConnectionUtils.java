package com.geekhub.season4.lesson08_Threads;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Utils class that contains useful method to interact with URLConnection
 */
public class ConnectionUtils {

    /**
     * Downloads content for specified URL and returns it as a byte array.
     * Should be used for small files only. Don't use it to download big files it's dangerous.
     *
     * @param url
     * @return
     * @throws IOException
     */
    public static byte[] getData(URL url) throws IOException {
        //implement me
        byte[] data = new byte[5 * 1024];
        int lengthRead;
        new DataInputStream(url.openStream()).read(data);

        try (DataInputStream dis = new DataInputStream(url.openStream())) {
            do {
                lengthRead = dis.read(data);
            } while (lengthRead > 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }
}
