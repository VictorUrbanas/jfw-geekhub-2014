package com.geekhub.season4.lesson03_Exceptions.car;

import com.geekhub.season4.lesson02_OOP.Engine;
import com.geekhub.season4.lesson02_OOP.GasTank;
import com.geekhub.season4.lesson02_OOP.Vehicle;

/**
 * Created by Vik on 02.11.2014.
 */
public class Car extends Vehicle {
    Engine engine;
    GasTank gasTank;
    int maxSpeed = 200;
    int curSpeed = 0;
    int fuelConsumption = 9;

    public Car(String vehicleType) {
        super(vehicleType);
    }

    @Override
    public void acceleration() throws Exception {
        if (!engine.isWorking()) {
            throw new Exception("Start driving please!");
        }
        if (engine.isDestroyed()) {
            throw new EngineException("Engine is destroyed!");
        }
        if (!gasTank.isFull()) {
            this.stop();
            throw new GasTankException("Gas-tank is empty!");
        }
        if (curSpeed == maxSpeed) {
            engine.upTemperature(2);
            this.driving(true);
            System.out.println("Be careful it is maximum speed, AAAAAA!");
        }
        System.out.println("Acceleration...");
        curSpeed += curSpeed + 10 > maxSpeed ? maxSpeed - curSpeed : 10;
        engine.upTemperature(2);
        this.driving(true);
    }

    public void deceleration() throws Exception {
        if (!engine.isWorking()) {
            throw new Exception("Start driving please!");
        }
        if (engine.isDestroyed()) {
            throw new EngineException("Engine is destroyed!");
        }
        if (!gasTank.isFull()) {
            this.stop();
            throw new GasTankException("Gas-tank is empty!");
        }
        curSpeed -= 10;
    }

    @Override
    public void turning(String rout) throws Exception {
        if (engine.isDestroyed()) {
            throw new EngineException("Engine is destroyed!");
        }
        if (!gasTank.isFull()) {
            this.stop();
            throw new GasTankException("Gas-tank is empty!");
        }
        System.out.format("Turning to the %s\n", rout);
        curSpeed -= 10;
    }

    @Override
    public void stop() {
        if (!engine.isWorking()) {
            System.out.println("Already stopped");
            return;
        }
        curSpeed = 0;
        engine.downTemperature(engine.getTemperature());
        engine.setWorking(false);
        System.out.println("Stopped...");
    }

    @Override
    public void inspectMechanism() {
        System.out.format(
            "fuel: %d\nengine temperature: %d\nspeed: %d (max speed: %d)\n",
                gasTank.getFuelValue(), engine.getTemperature(),
                curSpeed, maxSpeed
        );
    }

    @Override
    public boolean driving(boolean accelerate) throws Exception {
        if (engine.isDestroyed()) {
            throw new EngineException("Engine is destroyed!");
        }
        if (!gasTank.isFull()) {
            this.stop();
            throw new GasTankException("Gas-tank is empty!");
        }
        if (curSpeed == 0) {
            curSpeed = 40;
            engine.upTemperature(80);
            engine.setWorking(true);
            System.out.print("Start ");
        } else { // an hour ago
            gasTank.burnFuel(fuelConsumption);
        }
        System.out.println("Driving...");
        return true;
    }

    @Override
    public void activation() {
        engine = new Engine(130);
        gasTank = new GasTank(60);
    }

    public void refuel() {
        gasTank.upFuel(10);
    }
}
