package com.geekhub.season4.lesson03_Exceptions.car;

/**
 * Created by Vik on 08.11.2014.
 */
public class EngineException extends Exception {
    public EngineException(String message) {
        super(message);
    }
}
