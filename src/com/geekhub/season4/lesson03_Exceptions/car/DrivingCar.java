package com.geekhub.season4.lesson03_Exceptions.car;

import javax.swing.*;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Vik on 02.11.2014.
 */
public class DrivingCar {
    public static void main(String[] args) {
        if (JOptionPane.showConfirmDialog(null, "Do you need Calculator?",
                "Calculator?", JOptionPane.YES_NO_OPTION) == 0) {
                try {
                    Runtime.getRuntime().exec("calc");
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
        boolean exit = false;
        Car mersedes = new Car("mersedes Benz");
        mersedes.activation();
        while (!exit) {
            System.out.format(
                    "Choose option:\n" +
                            "1. Drive car\n" +
                            "2. Accelerating\n" +
                            "3. Decelerating\n" +
                            "4. Turn\n" +
                            "5. Stop\n" +
                            "6. show car data\n" +
                            "7. Refuel\n"
            );
            try {
                chooseOption(mersedes);
            }
            catch (GasTankException e) {
                System.out.println(e.getMessage());
                System.out.println("Maybe you need refuel?!");
            }
            catch (EngineException e) {
                System.out.println(e.getMessage());
                System.out.println("Try again!");
            }
            catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public static void chooseOption(Car mersedes) throws Exception{
        Scanner scanner = new Scanner(System.in);
        switch (scanner.nextInt()) {
            case 1:
                try {
                    mersedes.driving(false);
                } catch (Exception e) {
                    throw e;
                }
                break;
            case 2:
                try {
                    mersedes.acceleration();
                } catch (Exception e) {
                    throw e;
                }
                break;
            case 3:
                try {
                    mersedes.deceleration();
                } catch (Exception e) {
                    throw e;
                }
                break;
            case 4:
                try {
                    mersedes.turning(scanner.next());
                } catch (Exception e) {
                    throw e;
                }
                break;
            case 5:
                mersedes.stop();
                break;
            case 6:
                mersedes.inspectMechanism();
                break;
            case 7:
                mersedes.refuel();
                break;
            default:
                System.out.println("Illegal choise!");
        }
    }
}
