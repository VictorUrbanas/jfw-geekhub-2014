package com.geekhub.season4.lesson03_Exceptions.testConcatenation;

/**
 * Result
 * 14 milliseconds (StringBuffer)
 * 9 milliseconds (StringBuilder)
 * 53586 milliseconds (String)
 *
 * Created by Vik on 02.11.2014.
 */
public class testConcatenation {
    public static void main(String[] args) {
        String testString = "Ladies and gentlemen, it is";
        String timeInfo = "%d milliseconds (%s)\n";

       long time = System.currentTimeMillis();
        StringBuffer stringBuffer = new StringBuffer(testString);
        System.out.format(timeInfo, stringBufferTest(stringBuffer) - time,
                "StringBuffer");
        time = System.currentTimeMillis();

        StringBuilder stringBuilder = new StringBuilder(testString);
        System.out.format(timeInfo, stringBuilderTest(stringBuilder) - time,
                "StringBuilder");
        time = System.currentTimeMillis();

        System.out.format(timeInfo, stringTest(testString) - time, "String");
    }

    public static long stringBufferTest(StringBuffer sb) {
        for (int i = 0; i < 100000; i++) {
            sb.append(" very long test string!");
        }
        return System.currentTimeMillis();
    }

    public static long stringBuilderTest(StringBuilder sb) {
        for (int i = 0; i < 100000; i++) {
            sb.append(" very long test string!");
        }
        return System.currentTimeMillis();
    }

    public static long stringTest(String str) {
        for (int i = 0; i < 100000; i++) {
            str += " very long test string!";
        }
        return System.currentTimeMillis();
    }
}