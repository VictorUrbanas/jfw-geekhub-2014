package com.geekhub.season4.lesson03_Exceptions.compare;

/**
 * Created by Vik on 01.11.2014.
 */
public class Pond implements Comparable {
    private double depth;

    public Pond() {
        this.depth = new java.util.Random().nextDouble()*50;
    }

    @Override
    public int compareTo(Object o) {
        Pond anotherPond = (Pond) o;
        if (anotherPond.depth != this.depth) {
            return anotherPond.depth > this.depth ? 1 : -1;
        }
        return 0;
    }

    public double getDepth() {
        return depth;
    }
}