package com.geekhub.season4.lesson03_Exceptions.compare;

import java.util.Arrays;

/**
 * Created by Vik on 01.11.2014.
 */
public class inspectionPonds {
    public static void main(String[] args) {
        Pond[] ponds = new Pond[10];
        for (int i = 0; i < 10; i++) {
            ponds[i] = new Pond();
        }
        Pond[] pondsByDepth = (Pond[]) sort(ponds);
        String printPond = "Pond #%-2d has depth %.2f meters\n";
        for (int i = 0; i < 10; i++) {
            System.out.format(printPond, i + 1, pondsByDepth[i].getDepth());
        }
    }

    public static Comparable[] sort(Comparable[] elements) {
        Comparable[] sortElements = elements.clone();
        Arrays.sort(sortElements);
        return sortElements;
    }
}
