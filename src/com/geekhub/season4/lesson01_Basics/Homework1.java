package com.geekhub.season4.lesson01_Basics;

import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * This class contains methods for calculating factorial or first numbers
 * of Fibonacci sequence. Also here declared the method which print number
 * by word. All numbers should be entered from the keyboard.
 *
 * version 1.2.1
 * Created by Vik on 16.10.2014.
 */
public class Homework1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Exercise 1 (factorial)\nn = ");
        int n = scanner.nextInt();
        try {
            System.out.printf("%d! = %d\nExercise 2 (Fibonacci sequence)\nn = ", n, factorial(n));
            n = scanner.nextInt();
            System.out.printf("The first %d Fibonacci's numbers are %s\n" +
                    "Exercise 3 (number by word in different ways)\n", n,
                    Arrays.toString(fibonacci(n)));
            System.out.print("n = ");
            n = scanner.nextInt();
            scanner.close();
            printNumber(n);
        } catch (InputMismatchException e) {
            System.out.println("Please set integer values!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static int factorial(int n) throws Exception {
        if (n < 0) {
            throw new Exception("Factorial of negative numbers does not exists!");
        }
        if (n == 0 || n == 1) {
            return 1;
        }
        else {
            return factorial(n-1)*n;
        }
    }

    public static int[] fibonacci(int n) throws Exception {
        if (n < 1) {
            throw new Exception("N should be more than 0!");
        }
        int sequence[] = new int[n];
        sequence[0] = 0;
        if (n == 1) {
            return sequence;
        }
        sequence[1] = 1;
        if (n == 2) {
            return sequence;
        }
        for (int i = 2; i < n; i++) {
            sequence[i] = sequence[i - 1] + sequence[i - 2];
        }
        return sequence;
    }

    public static void printNumber(int inputNumber) throws Exception {
        if (inputNumber < 0 || inputNumber > 9) {
            throw new Exception("Please set number in range [0..9]!");
        }
        String numbers[] = {"zero", "one", "two", "three", "four", "five", "six",
                "seven", "eight", "nine"};
        System.out.printf("1) %d is the same that %s (array)\n", inputNumber,  numbers[inputNumber]);

        String numberWord;
        switch (inputNumber) {
            case 0:
                numberWord = "zero";
                break;
            case 1:
                numberWord = "one";
                break;
            case 2:
                numberWord = "two";
                break;
            case 3:
                numberWord = "three";
                break;
            case 4:
                numberWord = "four";
                break;
            case 5:
                numberWord = "five";
                break;
            case 6:
                numberWord = "six";
                break;
            case 7:
                numberWord = "seven";
                break;
            case 8:
                numberWord = "eight";
                break;
            default:
                numberWord = "nine";
        }
        System.out.printf("2) %d is the same that %s (case)\n", inputNumber,  numberWord);
        if (inputNumber == 0) {
            numberWord = "zero";
        } else if (inputNumber == 1) {
            numberWord = "one";
        } else if (inputNumber == 2) {
            numberWord = "two";

        } else if (inputNumber == 3) {
            numberWord = "three";

        } else if (inputNumber == 4) {
            numberWord = "four";

        } else if (inputNumber == 5) {
            numberWord = "five";

        } else if (inputNumber == 6) {
            numberWord = "six";

        } else if (inputNumber == 7) {
            numberWord = "seven";

        } else if (inputNumber == 8) {
            numberWord = "eight";

        } else {
            numberWord = "nine";
        }
        System.out.printf("3) %d is the same that %s (if)\n", inputNumber,  numberWord);
    }
}
