package com.geekhub.season4.lesson09_Db_Connectivity.storage;

import com.geekhub.season4.lesson09_Db_Connectivity.objects.Entity;
import com.geekhub.season4.lesson09_Db_Connectivity.objects.Ignore;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

/**
 * Implementation of {@link com.geekhub.season4.lesson09_Db_Connectivity.storage.Storage}
 * that uses database as a storage for objects.
 * It uses simple object type names to define target table to save the object.
 * It uses reflection to access objects fields and retrieve data to map to database tables.
 * As an identifier it uses field id of
 * {@link com.geekhub.season4.lesson09_Db_Connectivity.objects.Entity} class.
 * Could be created only with {@link java.sql.Connection} specified.
 */
public class DatabaseStorage implements Storage {
    private Connection connection;

    public DatabaseStorage(Connection connection) {
        this.connection = connection;
    }

    @Override
    public <T extends Entity> T get(Class<T> clazz, Integer id) throws Exception {
        //this method is fully implemented, no need to do anything, it's just an example
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM ").append(clazz.getSimpleName())
                .append(" WHERE id = ").append(id);
        System.out.format("> %s\n", sql.toString());
        try (Statement statement = connection.createStatement()) {
            List<T> result = extractResult(clazz, statement.executeQuery(sql.toString()));
            return result.isEmpty() ? null : result.get(0);
        }
    }

    @Override
    public <T extends Entity> List<T> list(Class<T> clazz) throws Exception {
        //implement me according to interface by using extractResult method
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT * FROM ").append(clazz.getSimpleName());
        System.out.format("> %s\n", sql.toString());
        try (Statement statement = connection.createStatement()) {
            return extractResult(clazz, statement.executeQuery(sql.toString()));
        }
    }

    @Override
    public <T extends Entity> boolean delete(T entity) throws Exception {
        //implement me
        try (Statement statement = connection.createStatement()) {
            StringBuilder sql = new StringBuilder();
            sql.append("DELETE FROM ").append(entity.getClass().getSimpleName())
                    .append(" WHERE id = ").append(entity.getId());
            System.out.format("> %s\n", sql.toString());
            int rowsAffected = statement.executeUpdate(sql.toString());
            return rowsAffected != 0;
        }
    }

    @Override
    public <T extends Entity> void save(T entity) throws Exception {
        Map<String, Object> data = prepareEntity(entity);

        StringBuilder sql = new StringBuilder();
        if (entity.isNew()) {
            //implement me
            //need to define right SQL query to create object
            sql.append("INSERT INTO ").append(entity.getClass().getSimpleName())
                    .append("(");
            Set<Map.Entry<String, Object>> dataSet = data.entrySet();
            for (Map.Entry<String, Object> entry : dataSet) {
                sql.append(entry.getKey()).append(", ");
            }
            //remove extra comma and space
            sql.replace(sql.length() - 2, sql.length(), "")
                    .append(") VALUES(");
            for (Map.Entry<String, Object> entry : dataSet) {
                Object val = entry.getValue();
                if (val instanceof String) {
                    val = "\"" + val + "\"";
                }
                sql.append(val).append(", ");
            }
            //remove extra comma and space
            sql.replace(sql.length() - 2, sql.length(), "")
                    .append(")");

        } else {
            //implement me
            //need to define right SQL query to update object
            sql.append("UPDATE ").append(entity.getClass().getSimpleName()).append(" SET ");
            int i = 0;
            for (Map.Entry<String, Object> entry : data.entrySet()) {
                sql.append(entry.getKey()).append(" = ");
                Object val = entry.getValue();
                if (val instanceof String) {
                    val = "\"" + val + "\"";
                }
                sql.append(val).append(", ");
            }
            //remove extra comma and space
            sql.replace(sql.length() - 2, sql.length(), "")
                    .append(" WHERE id = ").append(entity.getId());
        }
        System.out.format("> %s\n", sql.toString());
        //implement me, need to save/update object and update it with new id if it's a creation
        try(Statement statement = connection.createStatement()) {
            statement.executeUpdate(sql.toString(), Statement.RETURN_GENERATED_KEYS);
            if (entity.isNew()) {
                ResultSet generatedKeys = statement.getGeneratedKeys();
                if (generatedKeys.next()) {
                    entity.setId(generatedKeys.getInt(1));
                }
            }
        }catch (Exception sqlEx){
            sqlEx.printStackTrace();
        }
    }

    //converts object to map, could be helpful in save method
    private <T extends Entity> Map<String, Object> prepareEntity(T entity) throws Exception {
        //implement me
        Map<String, Object> preparedEntity = new HashMap<>();
        for (Field field : entity.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            if (field.isAnnotationPresent(Ignore.class)) {
                System.out.println("Ignore " + field.getName());
                continue;
            }
            preparedEntity.put(field.getName(), field.get(entity));
        }
        return preparedEntity;
    }

    //creates list of new instances of clazz by using data from resultset
    private <T extends Entity> List<T> extractResult(Class<T> clazz, ResultSet resultset) throws Exception {
        //implement me
        List<T> list = new ArrayList<>();
        while (resultset.next()) {
            T entity = clazz.newInstance();
            entity.setId(resultset.getInt("id"));
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                if (field.isAnnotationPresent(Ignore.class)) {
                    continue;
                }
                field.setAccessible(true);
                field.set(entity, resultset.getObject(field.getName()));
            }
            list.add(entity);
        }
        return list;
    }
}
