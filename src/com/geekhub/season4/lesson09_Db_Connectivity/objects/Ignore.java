package com.geekhub.season4.lesson09_Db_Connectivity.objects;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used by any
 * {@link com.geekhub.season4.lesson09_Db_Connectivity.storage.Storage}
 * implementation to identify fields
 * of {@link com.geekhub.season4.lesson09_Db_Connectivity.objects.Entity}
 * that need to be avoided from being stored
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Ignore {
}
