package com.geekhub.season4.lesson04_Collections.setOperations;

import java.util.Set;

/**
 * Created by Vik on 17.11.2014.
 */
public interface setOperations {
    public boolean equals(Set a, Set b);

    public Set union(Set a, Set b);

    public Set subtract(Set a, Set b);

    public Set intersect(Set a, Set b);

    public Set symmetricSubtract(Set a, Set b);
}