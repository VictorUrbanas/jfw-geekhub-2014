package com.geekhub.season4.lesson04_Collections.setOperations;

import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

/**
 * Created by Vik on 17.11.2014.
 */
public class SetOfNumbers {
    public static boolean exit = false;
    public static Scanner scanner = new Scanner(System.in);
    public static Set<Integer> allNumbers = new HashSet<>();
    public static Set<Integer> evenNumbers = new HashSet<>();
    public static Set<Integer> negativeNumbers = new HashSet<>();
    public static Set<Integer> notNegativeNumbers = new HashSet<>();
    public static Set<Integer> resultSet = new HashSet<>();
    public static NumberManipulate numberManipulate = new NumberManipulate();
    public static String chooseSetMenu =
            "choose set:\n1. even\n2. negative\n3. not negative";

    public static void main(String[] args) {
        int option;
        createGroups();
        while (!exit) {
            option = chooseOperation();
            if (option == 6) {
                printSet();
                continue;
            }
            getResult(option, chooseSets(), chooseSets());
            if (option == 1) {
                continue;
            }
            if (resultSet.size() == 0) {
                System.out.println("Ineffectually!");
                continue;
            }
            System.out.println("Your result:");
            for (Object element : resultSet) {
                System.out.format("%-7d", (Integer) element);
            }
        }
    }

    public static void createGroups() {
        System.out.println("How many rr would you like to create?");
        int quantity = scanner.nextInt();
        Integer element;
        Random random = new Random();
        System.out.println("Your rr");
        for (int i = 0; i < quantity; i++) {
            element = random.nextInt(15) * (random.nextBoolean() ? 1 : -1);
            System.out.format("%-7d", element);
            allNumbers.add(element);
            if (element % 2 == 0) {
                evenNumbers.add(element);
            }
            if (element < 0) {
                negativeNumbers.add(element);
            } else {
                notNegativeNumbers.add(element);
            }
        }
    }

    public static int chooseOperation() {
        System.out.println("\nchoose operation:\n1. equals\n2. union\n3. subtract\n" +
                "4. intersect\n5. symmetricSubtract\n6. Just print set");
        return scanner.nextInt();
    }

    public static Set<Integer> chooseSets() {
        System.out.println(chooseSetMenu);
        switch (scanner.nextInt()) {
            case 1:
                return evenNumbers;
            case 2:
                return negativeNumbers;
            case 3:
                return notNegativeNumbers;
            default:
                return chooseSets();
        }
    }

    public static void getResult(int option, Set<Integer> a, Set<Integer> b) {
        switch (option) {
            case 1:
                System.out.println(numberManipulate.equals(a, b));
                break;
            case 2:
                resultSet = numberManipulate.union(a, b);
                break;
            case 3:
                resultSet = numberManipulate.subtract(a, b);
                break;
            case 4:
                resultSet = numberManipulate.intersect(a, b);
                break;
            case 5:
                resultSet = numberManipulate.symmetricSubtract(a, b);
                break;
            default:
                System.out.format("Illegal choise! Try again!\n");
        }
    }

    public static void printSet() {
        Set<Integer> setForPrint;
        System.out.println(chooseSetMenu);
        switch (scanner.nextInt()) {
            case 1:
                setForPrint = evenNumbers;
                break;
            case 2:
                setForPrint = negativeNumbers;
                break;
            case 3:
                setForPrint = notNegativeNumbers;
                break;
            default:
                System.out.println("Illegal choise!");
                return;
        }
        for (Integer element : setForPrint) {
            System.out.format("%-7d", element);
        }
    }
}
