package com.geekhub.season4.lesson04_Collections.setOperations;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Vik on 17.11.2014.
 */
public class NumberManipulate implements setOperations {

    @Override
    public boolean equals(Set a, Set b) {
        return a.containsAll(b) && b.containsAll(a);

    }

    @Override
    public Set union(Set a, Set b) {
        Set resultSet = new HashSet(a);
        a.addAll(b);
        return resultSet;
    }

    @Override
    public Set subtract(Set a, Set b) {
        Set resultSet = new HashSet(a);
        resultSet.removeAll(b);
        return resultSet;
    }

    @Override
    public Set intersect(Set a, Set b) {
        Set resultSet = new HashSet(a);
        a.retainAll(b);
        return resultSet;
    }

    @Override
    public Set symmetricSubtract(Set a, Set b) {
        return subtract(union(a, b), intersect(a, b));
    }
}
