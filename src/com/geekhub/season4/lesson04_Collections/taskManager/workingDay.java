package com.geekhub.season4.lesson04_Collections.taskManager;

import java.util.*;

/**
 * Created by Vik on 18.11.2014.
 */
public class workingDay {
    public static Random random = new Random();
    public static NoteBook noteBook = new NoteBook();
    public static Date dateForRemove;

    public static void main(String[] args) {
        // test addTask method
        fillTaskList();

        // test removeTask method
        noteBook.removeTask(dateForRemove);

        // test getCategories method
        System.out.println("\nExisting categories:");
        int counter = 1;
        for (String category : noteBook.getCategories()) {
            System.out.format("%d. %s\n", counter++, category);
        }

        // test getTasksByCategory method
        counter = 1;
        System.out.println("\nTasks of category #2:");
        for (Task task: noteBook.getTasksByCategory("category #2")) {
            System.out.format("%d. %s\n", counter++, task.getDescription());
        }

        // test getTasksByCategories method
        System.out.println("\nTasks by categories:");
        for (Map.Entry<String, List<Task>> element : noteBook.getTasksByCategories().entrySet()) {
            System.out.format("Tasks of category \"%s\":\n", element.getKey());
            for (Task task : element.getValue()) {
                System.out.format("    %s\n", task.getDescription());
            }
        }

        // test getTasksForToday method
        System.out.println("\nTasks for today:");
        for (Task task : noteBook.getTasksForToday()) {
            System.out.format("    %s\n", task.getDescription());
        }

    }

    public static Date generateDate() {
        Calendar newDate = Calendar.getInstance();
        newDate.clear();
        newDate.set(Calendar.YEAR, random.nextInt(2) + 2014);
        newDate.set(Calendar.MONTH, random.nextInt(12));
        newDate.set(Calendar.DATE, random.nextInt(31));
        newDate.set(Calendar.HOUR, random.nextInt(24));
        newDate.set(Calendar.MINUTE, random.nextInt(60));
        newDate.set(Calendar.SECOND, 0);
        newDate.set(Calendar.MILLISECOND, 0);
        return newDate.getTime();
    }

    public static void fillTaskList() {
        Scanner scanner = new Scanner(System.in);
        String categoryTpl = "category #";
        String taskTpl = "taskDescription #";
        int i;
        System.out.println("How many tasks do you want to create?");
        int numberOfTasks = scanner.nextInt();
        System.out.println("How many categories do you want to create?");
        int numberOfCategories = scanner.nextInt();
        for (i = 0; i < numberOfTasks; i++) {
            noteBook.addTask(dateForRemove = generateDate(),
                    new Task(categoryTpl +
                            (random.nextInt(numberOfCategories)+1),
                            taskTpl + (i + 1)));
        }
        Calendar today = Calendar.getInstance();
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);
        noteBook.addTask(today.getTime(), new Task("some cat", "task for today"));
        System.out.println(today.getTime());
        System.out.format("%d tasks crated!\n", i);
    }
}
