package com.geekhub.season4.lesson04_Collections.taskManager;

/**
 * Created by Vik on 17.11.2014.
 */
public class Task {
    private String category;
    private String description;

    public Task(String category, String description) {
        this.category = category;
        this.description = description;
    }

    public String getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }

}
