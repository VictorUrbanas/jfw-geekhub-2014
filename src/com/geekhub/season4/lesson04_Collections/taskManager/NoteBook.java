package com.geekhub.season4.lesson04_Collections.taskManager;

import java.util.*;

/**
 * Created by Vik on 17.11.2014.
 */
public class NoteBook implements TaskManager {
    Map<Date, Task> taskList = new HashMap<>();

    @Override
    public void addTask(Date date, Task task) {
        taskList.put(date, task);
    }

    @Override
    public void removeTask(Date date) {
        System.out.format("removing ...\ncategory: %s\ndescription: %s\n",
                taskList.get(date).getCategory(),
                taskList.get(date).getDescription());
        taskList.remove(date);
    }

    @Override
    public Collection<String> getCategories() {
        List<String> categories = new ArrayList<>();
        String category;
        for (Task task : taskList.values()) {
            category = task.getCategory();
            if (!categories.contains(category)) {
                categories.add(category);
            }
        }
        return categories;
    }

    @Override
    public Map<String, List<Task>> getTasksByCategories() {
        Map<String, List<Task>> tasksByCategories =
                new HashMap<>();
        String category; // it will be category of each task
        List tasksOfCategory; // it will be tasks of each unique category
        for (Task task : taskList.values()) {
            category = task.getCategory();
            tasksOfCategory = tasksByCategories.get(category);
            if (tasksOfCategory != null) {
                tasksOfCategory.add(task);
            }
            else {
                tasksOfCategory = new ArrayList<>();
                tasksOfCategory.add(task);
                tasksByCategories.put(category, tasksOfCategory);
            }
        }
        return tasksByCategories;
    }

    @Override
    public List<Task> getTasksByCategory(String category) {
        List<Task> tasksByCategory = new ArrayList<>();
        for (Task task : taskList.values()) {
            if (task.getCategory().equals(category)) {
                tasksByCategory.add(task);
            }
        }
        return tasksByCategory;
    }

    @Override
    public List<Task> getTasksForToday() {
        List<Task> tasksForToday = new ArrayList<>();
        Calendar currentDate = Calendar.getInstance();
        currentDate.set(Calendar.SECOND, 0);
        currentDate.set(Calendar.MILLISECOND, 0);
        for (Map.Entry<Date, Task> task : taskList.entrySet()) {
            if (task.getKey().equals(currentDate.getTime())) {
                tasksForToday.add(task.getValue());
            }
        }
        return tasksForToday;
    }
}
