package com.geekhub.season4.lesson04_Collections.taskManager;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Vik on 17.11.2014.
 */
public interface TaskManager {
    public void addTask(Date date, Task task);
    public void removeTask(Date date);
    public Collection<String> getCategories();

    public Map<String, List<Task>> getTasksByCategories();
    public List<Task> getTasksByCategory(String category);
    public List<Task> getTasksForToday();

}
