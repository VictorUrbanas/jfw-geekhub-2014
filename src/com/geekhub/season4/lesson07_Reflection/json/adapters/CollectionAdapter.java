package com.geekhub.season4.lesson07_Reflection.json.adapters;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Collection;

/**
 * Converts all rr that extends java.util.Collections to JSONArray.
 */
public class CollectionAdapter implements JsonDataAdapter<Collection> {
    @Override
    public Object toJson(Collection c) throws JSONException{
        //implement me
        return new JSONArray(c);
    }
}
