package com.geekhub.season4.lesson07_Reflection.json.adapters;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Converts object of type java.util.Date to String by using dd/MM/yyyy format
 */
public class DateAdapter implements JsonDataAdapter<Date> {
    @Override
    public Object toJson(Date date) {
        //implement me
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        return String.format(dateFormat.format(date));
    }
}
