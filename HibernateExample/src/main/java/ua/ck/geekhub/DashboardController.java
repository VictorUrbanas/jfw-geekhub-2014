package ua.ck.geekhub;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DashboardController {

	@Autowired
	UserService userService;

	@Autowired
    GroupService groupService;

	@RequestMapping(value = "/", method = {RequestMethod.GET, RequestMethod.HEAD})
	public String redirectUsers(ModelMap model) {
		return "redirect:users";
	}

	@RequestMapping(value = "/users", method = {RequestMethod.GET, RequestMethod.HEAD})
	public String user(ModelMap model) {
		model.addAttribute("users", userService.getUsers());
        model.addAttribute("groups", groupService.getGroups());
		return "dashboard";
	}

	@RequestMapping(value = "/users", method = RequestMethod.POST)
	public String createUser(
			@RequestParam String firstName,
	        @RequestParam String lastName,
	        @RequestParam String email,
	        @RequestParam int groupId
	) {
        userService.createUser(firstName, lastName, email, groupService.getGroup(groupId));
		return "redirect:users";
	}

	@RequestMapping(value = "/groups", method = RequestMethod.POST)
	public String createGroup(@RequestParam String groupName) {
        groupService.createGroup(groupName);
		return "redirect:users";
	}

    @RequestMapping(value = "/user/edit", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String editUser(@RequestParam int id, ModelMap model) {
        model.addAttribute("user", userService.getUser(id));
        model.addAttribute("groups", groupService.getGroups());
        return "editUser";
    }

    @RequestMapping(value = "/user/update", method = RequestMethod.POST)
    public String updateUser(
            @RequestParam int id,
            @RequestParam String firstName,
            @RequestParam String lastName,
            @RequestParam String email,
            @RequestParam int groupId
    ) {
        userService.updateUser(id, firstName, lastName, email, groupService.getGroup(groupId));
        return "redirect:/users";
    }

    @RequestMapping(value = "/user/delete", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String deleteUser(
            @RequestParam int id
    ) {
        userService.deleteUser(userService.getUser(id));
        return "redirect:/users";
    }

    @RequestMapping(value = "/group/edit", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String editGroup(@RequestParam int id, ModelMap model) {
        model.addAttribute("group", groupService.getGroup(id));
        return "editGroup";
    }

    @RequestMapping(value = "/group/update", method = RequestMethod.POST)
    public String updateGroup(
            @RequestParam int id,
            @RequestParam String name
    ) {
        groupService.updateGroup(id, name);
        return "redirect:/users";
    }

    @RequestMapping(value = "/group/delete", method = {RequestMethod.GET, RequestMethod.HEAD})
    public String deleteGroup(
            @RequestParam int id
    ) {
        groupService.deleteGroup(groupService.getGroup(id));
        return "redirect:/users";
    }
}