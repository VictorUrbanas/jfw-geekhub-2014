<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 15/02/15
  Time: 21:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User edit</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/user/update" method="post">
    <fieldset>
        <legend>Edit user #${user.id}</legend>
        <label>
            <input type="hidden" name="id" value="${user.id}"/>
        </label>
        <label for="firstName">First name</label>
        <input type="text" name="firstName" id="firstName" value="${user.firstName}"/>
        <label for="lastName">First name</label>
        <input type="text" name="lastName" id="lastName" value="${user.lastName}"/>
        <label for="email">First name</label>
        <input type="email" name="email" id="email" value="${user.email}"/>
        <label for="group">Group</label>
        <select name="groupId" id="group">
            <option value="-1">Choose group</option>
            <c:forEach items="${groups}" var="group">
                <option value="${group.id}">${group.name}</option>
            </c:forEach>
        </select>
        <input type="submit"/>
    </fieldset>
</form>
</body>
</html>
