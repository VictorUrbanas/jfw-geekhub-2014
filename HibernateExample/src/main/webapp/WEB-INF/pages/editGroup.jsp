<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 15/02/15
  Time: 21:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Group edit</title>
</head>
<body>
    <form action="${pageContext.request.contextPath}/group/update" method="post">
        <fieldset>
            <legend>Edit group #${group.id}</legend>
            <label>
                <input type="hidden" name="id" value="${group.id}"/>
            </label>
            <label for="groupName">Group name</label>
            <input type="text" name="name" id="groupName" value="${group.name}"/>
            <input type="submit"/>
        </fieldset>
    </form>
</body>
</html>
