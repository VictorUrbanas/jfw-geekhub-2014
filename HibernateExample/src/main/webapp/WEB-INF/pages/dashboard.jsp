<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<body>
    <h1>Hibernate Example</h1>
    <form action="${pageContext.request.contextPath}/groups" method="post">
        <fieldset>
            <legend>Add a new group</legend>
            <label>
                <input type="text" name="groupName" placeholder="Group name"/>
            </label>
            <input type="submit"/>
        </fieldset>
    </form>
    <form action="${pageContext.request.contextPath}/users" method="post">
        <fieldset>
            <legend>Add a new user</legend>
            <label>
                <input name="firstName" placeholder="First Name">
            </label>
            <label>
                <input name="lastName" placeholder="Last Name">
            </label>
            <label>
                <input name="email" type="email" placeholder="Email">
            </label>
            <label>
                <select name="groupId">
                    <option value="-1">Choose group</option>
                    <c:forEach items="${groups}" var="group">
                        <option value="${group.id}">${group.name}</option>
                    </c:forEach>
                </select>
            </label>
            <input type="submit" value="Submit">
        </fieldset>
    </form>
    <c:choose>
        <c:when test="${not empty users}">
            <h2>Users</h2>
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>FIRST NAME</th>
                    <th>LAST NAME</th>
                    <th>EMAIL</th>
                    <th>GROUP</th>
                    <th>ACTION</th>
                </tr>

                <c:forEach items="${users}" var="user">
                    <tr>
                        <td>${user.id}</td>
                        <td>${user.firstName}</td>
                        <td>${user.lastName}</td>
                        <td>${user.email}</td>
                        <td>${user.group.name}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/user/edit?id=${user.id}">
                                Edit
                            </a>
                            <a href="${pageContext.request.contextPath}/user/delete?id=${user.id}" class="js-delete-entity">
                                Delete
                            </a>
                        </td>
                    </tr>
                </c:forEach>

            </table>
        </c:when>
        <c:otherwise>
            <p>There is no users in the database</p>
        </c:otherwise>
    </c:choose>
    <c:choose>
        <c:when test="${not empty groups}">
            <h2>Groups</h2>
            <table border="1">
                <tr>
                    <th>ID</th>
                    <th>NAME</th>
                    <th>ACTION</th>
                </tr>
                <c:forEach items="${groups}" var="group">
                    <tr>
                        <td>${group.id}</td>
                        <td>${group.name}</td>
                        <td>
                            <a href="${pageContext.request.contextPath}/group/edit?id=${group.id}">
                                Edit
                            </a>
                            <a href="${pageContext.request.contextPath}/group/delete?id=${group.id}" class="js-delete-entity">
                                Delete
                            </a>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:when>
        <c:otherwise>
            <p>There is no groups in the database</p>
        </c:otherwise>
    </c:choose>
    <script>
        document.addEventListener('DOMContentLoaded', function () {
            document.querySelector('.js-delete-entity').addEventListener('click', function (e) {
                if ( ! confirm('Are you sure you want to delete this entity?')) {
                    e.preventDefault();
                }
            });
        });
    </script>
</body>
</html>