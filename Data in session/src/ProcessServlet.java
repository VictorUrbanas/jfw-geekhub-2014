/**
 * Created by victor on 01/02/15.
 */

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name="process", urlPatterns={"/path"})
public class ProcessServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        if (req.getParameter("delete") != null) {
            doDelete(req, resp);
        }
        if (req.getParameter("session").equals("destroy")) {
            req.getSession().invalidate();
            req.getRequestDispatcher("/destroySession.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<Person> persons = (List<Person>) session.getAttribute("persons");
        if (persons == null) {
            persons = new ArrayList<>();
        }
        persons.add(new Person(req.getParameter("name"), req.getParameter("surname")));
        session.setAttribute("persons", persons);
        resp.sendRedirect("/");

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        List<Person> persons = (List<Person>) session.getAttribute("persons");

        int idToDelete = Integer.parseInt(req.getParameter("delete"));
        for (Person person : persons) {
            if (person.getId() == idToDelete) {
                persons.remove(person);
                break;
            }
        }
        session.setAttribute("persons", persons);
        resp.sendRedirect("/");

    }

}
