/**
 * Created by victor on 01/02/15.
 */

public class Person {
    private static int count = 0;

    private int id;
    private String name;
    private String surname;

    public Person(String name, String surname) {
        this.id = count++;
        this.name = name;
        this.surname = surname;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }

}
