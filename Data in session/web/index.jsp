<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 01/02/15
  Time: 13:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title></title>
    <style>
        table, th, td {
            border: 1px solid #000;
        }
        table {
            text-align: center;
            margin: 5px 0;
        }
        input[type=text] {
            background: #ccc;
            margin: 5px;
        }
    </style>
</head>
<body>
<h1>Persons</h1>
<a href="/path?session=destroy" class="js-session-destroy">Destroy session ...</a>
<form action="/path" method="post">
    <table >
        <tr>
            <th>Name</th>
            <th>Surname</th>
            <th>Action</th>
        </tr>
        <c:forEach items="${sessionScope.get('persons')}" var="person">
            <tr>
                <td>${person.name}</td>
                <td>${person.surname}</td>
                <td><a href="/path?delete=${person.id}" class="js-delete">delete</a></td>
            </tr>
        </c:forEach>
        <tr>
            <td>
                <label>
                    <input type="text" name="name" required="required"/>
                </label>
            </td>
            <td>
                <label>
                    <input type="text" name="surname" required="required"/>
                </label>
            </td>
            <td>
                <input type="submit" value="add"/>
            </td>
        </tr>
    </table>
</form>
<script src="/js/jquery-2.1.3.min.js"></script>
<script>
    $(document).ready(function () {
        $('.js-delete').on('click', function (e) {
            if ( ! confirm('Do you sure you want to remove it?')) {
                e.preventDefault();
            }
        });
        $('.js-session-destroy').on('click', function (e) {
            if ( ! confirm('All data will be removed! Do you sure you want to destroy session?')) {
                e.preventDefault();
            }
        });
    });
</script>
</body>
</html>
