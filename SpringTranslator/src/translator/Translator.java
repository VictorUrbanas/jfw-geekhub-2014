package translator;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Created by victor on 07/02/15.
 */

public class Translator {
    public static void main(String[] args) {
        ApplicationContext appContext = new ClassPathXmlApplicationContext("app-context.xml");
        Translation translation = appContext.getBean(Translation.class);
        System.out.println(translation.getTranslate());
    }
}
