package translator;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by victor on 07/02/15.
 */

public class Translation {
    private String originalText;
    private String langOut;
    private String translatedText;

    public Translation (String sourcePath, String langOut) {
        readOriginalText(sourcePath);
        this.langOut = langOut;
    }

    public void readOriginalText (String sourcePath) {
        try {
            String tempString;
            StringBuilder sb = new StringBuilder();
            BufferedReader bufferedReader = new BufferedReader(new FileReader(sourcePath));
            while ((tempString = bufferedReader.readLine()) != null) {
                sb.append(tempString);
            }
            originalText = sb.toString();
        } catch (FileNotFoundException e) {
            System.out.printf("\n%s\nPlease, try again.\nResult:\n", e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getTranslate () {
        StringBuilder sb = new StringBuilder();
        try {
            String tempString;
            URLConnection urlConnection = new URL(TranslatorConfig.URL_REQUEST +
                    "&text=" + encodeText(originalText) + "&lang=" + langOut).openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while ((tempString = bufferedReader.readLine()) != null) {
                sb.append(tempString);
            }
            translatedText = parseContent(sb.toString());

        } catch (IOException e) {
            e.printStackTrace();
        }
        return translatedText;
    }

    private String parseContent(String content) {
        String textTag = "text>";
        int lengthTextTag = textTag.length();
        int beginIndex = content.indexOf(textTag) + textTag.length();
        int endIndex = content.indexOf("/" + textTag) - 1;
        return content.substring(beginIndex, endIndex);
    }

    private String encodeText(String text) {
        String encodeURL = null;
        try {
            encodeURL = URLEncoder.encode(text, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.out.println("Sorry, but your URL cannot be encode!");
        }
        return encodeURL;
    }

    public String getTranslatedText() {
        return translatedText;
    }
}
