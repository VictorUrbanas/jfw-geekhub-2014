/**
 * Created by victor on 31/01/15.
 */

$().ready(function () {
    $('.js-trash').on('click', function (e) {
        if ( ! confirm('Are you sure you want to remove this file or directory?')) {
            e.preventDefault();
        }
    });
    var search = document.location.search;
    search = search.slice(1, search.length);
    var params = search.split('&');
    var isInfo = false;
    if (params.indexOf('removed=true') != -1) {
        $('h1').after($('<p class="success info-message">File successfully removed!</p>'));
        isInfo = true;
    }
    if (params.indexOf('removed=false') != -1) {
        $('h1').after($('<p class="error info-message">File can not be removed!</p>'))
        isInfo = true;
    }
    if (params.indexOf('created=true') != -1) {
        $('h1').after($('<p class="success info-message">File successfully created!</p>'))
        isInfo = true;
    }
    if (params.indexOf('created=false') != -1) {
        $('h1').after($('<p class="error info-message">File can not be created!</p>'))
    }
});