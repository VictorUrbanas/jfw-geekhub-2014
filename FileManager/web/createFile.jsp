<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 31/01/15
  Time: 20:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>File manager - create ${fileType}</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/css/style.css"/>
</head>
<body>
<h1>Create a new ${fileType}</h1>
<form action="/create?fileType=${fileType}" method="post">
  <fieldset>
    <legend>Create ${fileType}</legend>
    <ul>
      <li>
        <label for="name">Name</label>
        <input type="text" id="name" name="name"/>
      </li>
      <li>
        <label for="path">Where you want to create ${fileTpe}</label>
        <input type="text" id="path" name="path"/>
        <span>Please don't forget / in the end of the path</span>
      </li>
      <c:if test="${fileType eq 'file'}">
        <label for="content"></label>
        <textarea name="content" id="content" cols="30" rows="10"></textarea>
      </c:if>
      <li>
        <input type="submit" value="create" />
      </li>
    </ul>
  </fieldset>
</form>
<script src="/js/jquery-2.1.3.min.js"></script>
<script src="/js/ui.js"></script>
</body>
</html>
