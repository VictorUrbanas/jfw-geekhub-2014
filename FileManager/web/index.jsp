<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 25/01/15
  Time: 17:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>File manager</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/css/style.css" />
</head>
<body>
<h1>Welcome to the file manager!</h1>
<form action="/path/" method="post">
  <fieldset>
    <legend>Root folder</legend>
    <ul>
      <li>
        <label for="root">Please, set path to your folder...</label>
        <input type="text" id="root" name="rootFolder" />
      </li>
      <li>
        <input type="submit"/>
      </li>
    </ul>
  </fieldset>
</form>
</body>
</html>