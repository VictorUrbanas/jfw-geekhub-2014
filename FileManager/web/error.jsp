<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 31/01/15
  Time: 20:54
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>File manager error</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/css/style.css"/>
</head>
<body class="error-page">
<h1>Fatal error!</h1>
<h2>${errorMessage}</h2>
<img src="/images/error.png" alt="error image" class="error-image"/>
</body>
</html>
