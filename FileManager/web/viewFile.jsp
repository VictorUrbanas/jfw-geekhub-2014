<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 30/01/15
  Time: 00:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page pageEncoding="UTF-8" %>
<html>
<head>
    <title>File manager - view file</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/css/style.css"/>
</head>
<body>
<h1>View File</h1>
${contentFile}
</body>
</html>
