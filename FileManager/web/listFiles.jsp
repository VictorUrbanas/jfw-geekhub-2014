<%--
  Created by IntelliJ IDEA.
  User: victor
  Date: 25/01/15
  Time: 22:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>File manager - list files</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="/css/style.css"/>
</head>
<body>
  <h1>List of files</h1>
  <h2>Do you want to create file or directory here?</h2>
  <a href="/create${currentPath}?fileType=dir" class="create-dir">Create new directory</a>
  <a href="/create${currentPath}?fileType=file" class="create-file">Create new file</a>
  <ul class="sourceList">
    <li class="backLink"><a href="/path${backLink}?fileTypeOpen=dir">..</a></li>
    <c:forEach items = "${files}" var = "fileOrDir">
        <li class="${fileOrDir.fileType}">
            <a href="/trash${fileOrDir.link}" class="trash js-trash action">trash</a>
            <c:choose>
                <c:when test="${fileOrDir.fileType eq 'dir'}">
                    <a href="/path${fileOrDir.link}?fileTypeOpen=${fileOrDir.fileType}">${fileOrDir.name}</a>
                </c:when>
                <c:otherwise>
                    <a href="/view${fileOrDir.link}?fileTypeOpen=${fileOrDir.fileType}">${fileOrDir.name}</a>
                </c:otherwise>
            </c:choose>
            <c:if test="${fileOrDir.fileType eq 'file'}">
                <a href="/edit${fileOrDir.link}" class="edit action">edit</a>
                <%--<a href="/view${fileOrDir.link}" class="view action">view</a>--%>
            </c:if>
      </li>
    </c:forEach>
  </ul>
  <script src="/js/jquery-2.1.3.min.js"></script>
  <script src="/js/ui.js"></script>
</body>
</html>
