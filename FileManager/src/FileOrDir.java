/**
 * Created by victor on 26/01/15.
 */

public class FileOrDir {
    private String name;
    private String fileType;
    private String link;

    public FileOrDir (String name, String path) {
        this.name = name;
        this.link = path;
    }

    public String getFileType() {
        return fileType;
    }
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }
    public void setLink(String link) {
        this.link = link;
    }
}
