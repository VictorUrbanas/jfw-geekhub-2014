/**
 * Created by victor on 31/01/15.
 */

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet(name="createFile", urlPatterns = {"/create/*"})
public class CreteFileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String fileType = req.getParameter("fileType");
        if (fileType.equals("dir") || fileType.equals("file")) {
            req.setAttribute("fileType", req.getParameter("fileType"));
            req.getRequestDispatcher("/createFile.jsp").forward(req, resp);
        } else {
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String fileType = req.getParameter("fileType");
        String path = req.getParameter("path");
        File newFile = new File(path + req.getParameter("name"));
        switch (fileType) {
            case "dir":
                resp.sendRedirect(
                        "/path" + path + "?fileTypeOpen=dir&created=" + newFile.mkdir()
                );
                break;
            case "file":
                resp.sendRedirect(
                        "/path" + path + "?fileTypeOpen=dir&created=" + newFile.createNewFile()
                );
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                        new FileOutputStream(path + req.getParameter("name")), "UTF-8"
                );
                outputStreamWriter.write(req.getParameter("content"));
                System.out.println(outputStreamWriter.getEncoding());
                outputStreamWriter.flush();
                outputStreamWriter.close();
                break;
            default:
                req.getRequestDispatcher("/error.jsp").forward(req, resp);
                break;
        }
    }
}
