/**
 * Created by victor on 25/01/15.
 */

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.*;
import java.io.File;
import java.io.IOException;

@WebServlet(name="FileManager", urlPatterns = {"/path/*"})
public class FileManagerServlet extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        req.setAttribute("backLink", new File(req.getPathInfo()).getParentFile());
        req.setAttribute("currentPath", req.getPathInfo());
        req.setAttribute("files", FilesOrDirs.getFiles(req.getPathInfo()));
        req.getRequestDispatcher(
                req.getParameter("fileTypeOpen").equals("file") ? "/viewFile.jsp" : "/listFiles.jsp"
        ).forward(req, resp);
    }

    @Override
    public void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.sendRedirect("/path" + req.getParameter("rootFolder") + "?fileTypeOpen=dir");
//        doGet(req, resp);
    }
}