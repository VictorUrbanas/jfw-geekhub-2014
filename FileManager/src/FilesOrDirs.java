import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by victor on 29/01/15.
 */
public class FilesOrDirs {
    public static List<FileOrDir> getFiles(String folder) {
        List<FileOrDir> files = new ArrayList<>();
        for (File item : java.nio.file.Paths.get(folder).toFile().listFiles()) {
            FileOrDir fileOrDir = new FileOrDir(item.getName(), folder + "/" +item.getName());
            fileOrDir.setFileType(item.isDirectory() ? "dir" : "file");
            files.add(fileOrDir);
        }
        return files;
    }
}
