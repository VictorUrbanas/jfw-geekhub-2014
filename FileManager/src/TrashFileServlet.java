import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

/**
 * Created by victor on 30/01/15.
 */
@WebServlet(name="TrashServlet", urlPatterns = {"/trash/*"})
public class TrashFileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        doDelete(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        File fileToRemove = new File(req.getPathInfo());
        resp.sendRedirect(
                "/path" + new File(req.getPathInfo()).getParentFile().getPath() +
                        "?fileTypeOpen=dir&removed=" + fileToRemove.delete()
        );
    }
}
