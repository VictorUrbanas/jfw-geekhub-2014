/**
 * Created by victor on 01/02/15.
 */

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

@WebServlet(name = "viewFile", urlPatterns = {"/view/*"})
public class ViewFileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        StringBuilder sb = new StringBuilder();
        InputStreamReader in = new InputStreamReader(new FileInputStream(new File(req.getPathInfo())), "UTF-8");
        System.out.println(in.read());
        try (BufferedReader br = new BufferedReader(new FileReader(new File(req.getPathInfo())))) {
            String readLine;
            while ((readLine = br.readLine()) != null) {
                sb.append(readLine);
            }
            br.close();
            req.setAttribute("contentFile", sb.toString());
            req.getRequestDispatcher("/viewFile.jsp").forward(req, resp);
        } catch (Exception e) {
            req.setAttribute("errorMessage", e.getMessage());
            resp.setContentType("text/html");
            resp.setCharacterEncoding("utf-8");
            req.getRequestDispatcher("/error.jsp").forward(req, resp);
        }

    }
}
